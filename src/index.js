import store from "./components/redux/state";
import "./index.css";
import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));

let rerenderEntireTree = (state) => {
  root.render(<App state={store.getState()} dispatch={store.dispatch.bind(store)} store={store}/>);
};


rerenderEntireTree(store.getState());

store.subscribe(rerenderEntireTree);
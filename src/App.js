import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import Header from "./components/Header/Header";
import Nav from "./components/Navbar/NavBar";
import Profile from "./components/Profile/Profile";
import Dialogs from "./components/Dialogs/Dialogs";
import News from "./components/News/News";
import Music from "./components/Music/Music";
import Settings from "./components/Settings/Settings";
import SideBar from "./components/sidebar/sidebar";

const App = (props) => {
  return (
    <BrowserRouter>
      <div className="appWrapper">
        <Header />
        <Nav />
        <div className="app-wrapper-content">
          <Route
            path="/profile"
            render={() => <Profile profilePage={props.state.profilePage}  dispatch={props.dispatch}/>}
          />
          <Route
            path="/dialogs"
            render={() => <Dialogs store={props.store}/>}
          />
          <Route path="/news" component={News} />
          <Route path="/music" component={Music} />
          <Route path="/settings" component={Settings} />
          <Route
            path="/friends"
            render={() => <SideBar state={props.state.sidebar} />}
          />
        </div>
      </div>
    </BrowserRouter>
  );
};

export default App;

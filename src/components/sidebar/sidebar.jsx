import React from "react";
import s from "./SideBar.module.css";
import Friend from "./Friend";

const SideBar = (props) => {
  let friendsElements = props.state.friends.map((friend) => (
    <Friend id={friend.id} name={friend.name} />
  ));

  return <div>{friendsElements}</div>;
};

export default SideBar;

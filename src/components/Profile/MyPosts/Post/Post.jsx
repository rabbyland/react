import React from "react";
import s from "./Post.module.css";

const Post = (props) => {
  return (
    <div className={s.item}>
      <img src="imgs/ava.png" />
      {props.message}
      <div>
        <span>{props.count}</span>
      </div>
    </div>
  );
};

export default Post;

import profileReducer from "./profile-reducer";
import messagesReducer from "./messages-reducer";
import sidebarReducer from "./sidebar-reducer";


let store = {
  _state: {
    profilePage: {
      posts: [
        { id: 1, message: "message1", count: "like 15" },
        { id: 2, message: "message2", count: "like 25" },
        { id: 3, message: "message3", count: "like 35" },
        { id: 4, message: "message4", count: "like 45" },
        { id: 5, message: "message5", count: "like 55" },
      ],
      newPostText: "Enterpost",
    },
    messagesPage: {
      dialogs: [
        { id: 1, name: "name1" },
        { id: 2, name: "name2" },
        { id: 3, name: "name3" },
        { id: 4, name: "name4" },
        { id: 5, name: "name5" },
      ],
      messages: [
        { id: 1, message: "message1" },
        { id: 2, message: "message2" },
        { id: 3, message: "message3" },
        { id: 4, message: "message4" },
        { id: 5, message: "message5" },
      ],
      newMessageBody: "",

    },
    sidebar: {
      friends: [
        { id: 1, name: "name1" },
        { id: 2, name: "name2" },
        { id: 3, name: "name3" },
        { id: 4, name: "name4" },
        { id: 5, name: "name5" },
      ],
    },
  },
  _callSubscriber() {
    console.log("state changed");
  },

  getState() {
    return this._state;
  },
  subscribe(observer) {
    this._callSubscriber = observer;
  },

  dispatch(action) {
    // debugger;
    // {type: 'ADD-POST'}

    this._state.profilePage = profileReducer(this._state.profilePage, action);
    this._state.messagesPage = messagesReducer(this._state.messagesPage, action);
    this._state.sidebar = sidebarReducer(this._state.sidebar, action);

    this._callSubscriber(this._state);

  },
};

export default store;
window.store = store;

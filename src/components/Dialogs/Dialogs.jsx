import React from "react";
import s from "./Dialogs.module.css";
import DialogItem from "./DialogItem/DialogItem";
import Message from "./DialogMessage/Message";
import { updateNewMessageBodyCreator, sendMessageCreator } from "../redux/messages-reducer";

const Dialogs = (props) => {

  let state = props.store.getState().messagesPage;

  let dialogsElements = state.dialogs.map((dialog) => (
    <DialogItem name={dialog.name} id={dialog.id} />
  ));

  let messageElements = state.messages.map((message) => (
    <Message id={message.id} message={message.message} />
  ));

  let newMessageBody = state.newMessageBody;

  let onSendMessageClick = () => {
    props.store.dispatch(sendMessageCreator());
  };

  let onNewMessageChange = (e) => {
    // debugger;
    let body = e.target.value;
    props.store.dispatch(updateNewMessageBodyCreator(body));
  };


  return (
    <div className={s.dialogs}>
      <div className={s.dialogsItems}>{dialogsElements} </div>
      <div className={s.messages}>{messageElements} </div>
      <div>
        <div>
          <textarea
            placeholder="Enter your message"
            onChange={onNewMessageChange}
            // ref={newPostElement}
            value={newMessageBody}
          />
        </div>
        <button onClick={onSendMessageClick}>Add post</button>
        <button>Remove post</button>
      </div>
    </div>
  );
};

export default Dialogs;
